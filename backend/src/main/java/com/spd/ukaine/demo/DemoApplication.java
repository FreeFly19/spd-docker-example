package com.spd.ukaine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.ResultSet;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}

@RestController
class HelloController {
    private final JdbcTemplate jdbcTemplate;

    public HelloController(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.execute("DROP TABLE IF EXISTS messages");
        jdbcTemplate.execute("CREATE TABLE messages(id bigserial primary key, text text)");
        jdbcTemplate.execute("insert into messages(text) values ('Hello, world!!!')");
    }

    @GetMapping("/api/hello")
	public MessageDto hello() {
		return jdbcTemplate.query("select text from messages", (ResultSet r) -> {
		    r.next();
		    return new MessageDto(r.getString("text"));
        });
	}
}

class MessageDto {
    private final String text;

    public MessageDto(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}